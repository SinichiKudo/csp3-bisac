import {combineReducers} from 'redux'
import {createStore, applyMiddleware} from 'redux'
import {composeWithDevTools} from 'redux-devtools-extension'
import {cartReducer}  from './reducers/cartReducer'
import thunk from 'redux-thunk'
import { addProductReviewReducer, getAllProductsReducer, getProductByIdReducer, updateProductReducer } from './reducers/productReducers'
import { placeOrderReducer } from './reducers/orderReducer'

const finalReducer = combineReducers({
    getAllProductsReducer : getAllProductsReducer,
    getProductByIdReducer : getProductByIdReducer,
    cartReducer : cartReducer,
    placeOrderReducer : placeOrderReducer,
    addProductReviewReducer : addProductReviewReducer,
    updateProductReducer : updateProductReducer

})

const cartItems = localStorage.getItem('cartItems') ? JSON.parse(localStorage.getItem('cartItems')) : []

const initialState = 
{
  cartReducer : {cartItems : cartItems}
}



const composeEnhancers = composeWithDevTools({
    
  });

const store = createStore(finalReducer, initialState, composeEnhancers(
    applyMiddleware(thunk)
    // other store enhancers if any
  ))

  export default store