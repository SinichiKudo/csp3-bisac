import React,{useEffect, useState} from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getProductById } from '../actions/productActions'
import { Row, Col, Card } from 'react-bootstrap'
import { Button } from 'react-bootstrap'
import { addToCart } from '../actions/addToCart'
import Review from '../Components/Review'


const ProductDescription = ({match}) =>
{
    const productid = match.params.id
    const dispatch = useDispatch()

    const getproductyidstate = useSelector(state =>state.getProductByIdReducer)
    const {product,loading,error} = getproductyidstate

    const[quantity, setQuantity] = useState(1)

    const quantityHandler = event =>
    {
        setQuantity(event.target.value)
    }

    const addtocartHandler = () =>
    {
        dispatch(addToCart(product, quantity))
    }

    useEffect(()=>
    {
       dispatch(getProductById(productid))
    },[dispatch,productid])
   
    return(
        <>
        {loading ? (<h1>Loading....</h1>) : error ? (<h1>Something went wrong</h1>) : (
            <>
            <Row>
                <Col sm={12} lg={6}>
                <Card className="m-3" >
                    <Card.Img variant="top" src={product.image} className="img-fluid"/>
                    <Card.Body>
                        <Card.Title>{product.name}</Card.Title>
                        <Card.Text>
                             {product.description}
                        </Card.Text>
                    </Card.Body>
                    </Card>
                </Col>
                <Col sm={12} lg={6}>
                    <div className="m-2">
                        <h3>Price ${product.price}</h3>
                        <hr/>
                        <h3>Quantity</h3>
                        <select value={quantity} onChange={quantityHandler}>{[...Array(product.countInStock).keys()].map((x,i)=>{
                            return <option value={i+1}>{i+1}</option>
                        })}</select>

                        <hr/>
                        <Button variant="primary" onClick={addtocartHandler}>Add to Cart</Button>
                    </div>
                    <hr/>
                    <Review key={product._id} product={product} />
                </Col>
            </Row>
            </>
        )}
        </>
    )
}

export default ProductDescription