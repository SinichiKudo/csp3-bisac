import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import {Row,Col,Table}  from 'react-bootstrap'
import { addToCart } from '../actions/addToCart'
import { deleteFromCart } from '../actions/addToCart'
import { Button } from 'react-bootstrap'
import Swal from 'sweetalert2'
// import Checkout from '../Components/Checkout'

const CartScreen = () =>
{

    const cartreducerstate = useSelector(state=>state.cartReducer)
    const dispatch = useDispatch()
    const {cartItems} = cartreducerstate

    var subtotal = cartItems.reduce((acc,item)=> acc + (item.price * item.quantity), 0)

    const order = () =>
    {
        Swal.fire({
            icon : "success",
            title : "Product Ordered Successfully",
            text : "Thank you"
        })
    }

    return(
        <>
        <Row className="mt-3 justify-content-center">
            <Col>
                <Table className="striped bordered hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Quantity</th>
                            <th>Total Price</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody>
                            {cartItems.map(item=>
                            {
                                return <tr>
                                    <td>{item.name}</td>
                                    <td>{item.price}</td>
                                    <td><select value={item.quantity} onChange={(e=>{dispatch(addToCart(item,e.target.value))})}>{[...Array(item.countInStock).keys()].map((x,i)=>{
                                        return <option value={i+1}>{i+1}</option>
                                    })}</select></td>
                                    <td>{item.quantity * item.price}</td>
                                    <td><i class="fas fa-trash-alt" onClick={()=>dispatch(deleteFromCart(item))}></i></td>
                                </tr>
                            })}
                        </tbody>
                </Table>
                <hr/>
                <h3 className="text-center">SubTotal : ${subtotal}</h3>
                <hr/>
                <div className="text-center">
                 <Button variant="primary" onClick={order} className="text-center">Buy Now</Button>     
                 </div> 
            </Col>
        </Row>
        </>
    )
}

export default CartScreen