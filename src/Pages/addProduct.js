import React,{useState,useEffect,useContext} from 'react'
import {Form, Button, Card} from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Redirect } from 'react-router-dom'
import UserContext from '../userContext'
import { Container } from 'react-bootstrap'


const AddProducts = () =>
{
    const {user} = useContext(UserContext)
    const [name,setName] = useState('')
    const[description,setDescription] = useState('')
    const[price,setPrice] = useState('')
    const[category , setCategory]= useState('')
    const [countInStock, setCountInStock] = useState('')
    const [image, setImage] = useState('')
    const [rating, setRating] = useState('')
    const[isActive, setIsActive] = useState('')
    const[willRedirect, setWillRedirect] = useState(false)

    const nameHandler = event =>
    {
        setName(event.target.value)
    }

    const descriptionHandler =event =>
    {
        setDescription(event.target.value)
    }
    
    const priceHandler = event =>
    {
        setPrice(event.target.value)
    }

    const categoryHandler = event =>
    {
        setCategory(event.target.value)
    }

    const countInStockHandler = event =>
    {
        setCountInStock(event.target.value)
    }

    const imageHandler = event =>
    {
        setImage(event.target.value)
    }

    const ratingHandler = event =>
    {
        setRating(event.target.value)
    }


    useEffect(()=>
    {
        if(name !== " " && description !== "" && price !== "")
        {
            setIsActive(true)
        }
        else
        {
            setIsActive(false)
        }
    },[name,description,price])

    const submitHandler =event =>
    {
        event.preventDefault()
        setName('')
        setDescription('')
        setPrice('')
        setCategory('')
        setImage('')
        setRating('')
        setCountInStock('')

        fetch("https://infinite-plains-24207.herokuapp.com/api/products/create",{
            method : "POST",
            headers :
            {
                "Content-Type" : "application/json",
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            },
            body : JSON.stringify({
                name : name,
                description : description,
                price : price,
                image : image,
                countInStock : countInStock,
                rating : rating,
                category : category
            })
        })
        .then(res =>res.json())
        .then(data =>
            {
                console.log(data)

                if(data.message)
                {
                    setWillRedirect(true)
                    Swal.fire({
                        icon : "success",
                        title : "Product Added Successfuly",
                        text : "Product Added"
                    })
                   
                }
                else
                {
                   
                    Swal.fire({
                        icon : "error",
                        title : "Creation of Products Failed",
                        text : data.message
                    })
                   
                }
            })
    }

    return (
        user.isAdmin || willRedirect
        ?
        <>
        <Container>
        <Card className="p-3 mt-3 mb-3">
        <h2 className="text-center my-2">Add Product</h2>
            <Form onSubmit={submitHandler}>
                <Form.Group className="mb-3" >
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control type="text" value={name} onChange={nameHandler} placeholder="Enter product name" /> 
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" value={description} onChange={descriptionHandler} placeholder="Description" />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="Number" value={price} onChange={priceHandler}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Image</Form.Label>
                    <Form.Control type="text" value={image} onChange={imageHandler}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Stocks</Form.Label>
                    <Form.Control type="Number" value={countInStock} onChange={countInStockHandler}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Category</Form.Label>
                    <Form.Control type="text" value={category} onChange={categoryHandler}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>rating</Form.Label>
                    <Form.Control type="Number" value={rating} onChange={ratingHandler}/>
                </Form.Group>
                {
                    isActive
                    ?  <Button className="btn btn-success btn-lg  mb-2" variant="primary" type="submit">Add Product</Button>
                    :   <Button className="btn btn-success btn-lg  mb-2" variant="danger" disabled >Add Product</Button>
                }
            </Form>
            </Card>
        </Container>
        </>
        :
        <Redirect to='/' />
    )
}

export default AddProducts