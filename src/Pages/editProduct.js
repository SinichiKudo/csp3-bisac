import React, {useState, useEffect} from 'react'
import { getProductById, updateProduct } from '../actions/productActions'
import { useDispatch, useSelector } from 'react-redux'
import { Form, Button, Card } from 'react-bootstrap'
// import { updateProductReducer } from '../reducers/productReducers'
import Load from '../Components/Loader'


const UpdateProduct = ({match}) =>
{
    const dispatch = useDispatch()
    const productstate = useSelector(state=>state.getProductByIdReducer)

    const{product, error, loading} = productstate

    const updateproductstate = useSelector((state)=>state.updateProductReducer)

    const {success, updateerror, updateloading} = updateproductstate

    const [name,setName] = useState('')
    const[description,setDescription] = useState('')
    const[price,setPrice] = useState('')
    const[category , setCategory]= useState('')
    const [countInStock, setCountInStock] = useState('')
    const [image, setImage] = useState('')
    const [isActive, setIsActive] = useState('')

    const nameHandler = event =>
    {
        setName(event.target.value)
    }

    const descriptionHandler =event =>
    {
        setDescription(event.target.value)
    }
    
    const priceHandler = event =>
    {
        setPrice(event.target.value)
    }

    const categoryHandler = event =>
    {
        setCategory(event.target.value)
    }

    const countInStockHandler = event =>
    {
        setCountInStock(event.target.value)
    }

    const imageHandler = event =>
    {
        setImage(event.target.value)
    }

    useEffect(()=>
    {

        if(product)
        {
            if(product._id === match.params.productid)
            {
                setName(product.name)
                setDescription(product.description)
                setPrice(product.price)
                setCategory(product.category)
                setImage(product.image)
                setCountInStock(product.countInStock)
            }   
            else
            {
                dispatch(getProductById(match.params.productid))
            }
            
        }
        else
        {
            dispatch(getProductById(match.params.productid))
        }
        
    },[dispatch, product])

    useEffect(()=>
    {
        if(name !== " " && description !== "" && price !== "")
        {
            setIsActive(true)
        }
        else
        {
            setIsActive(false)
        }
    },[name,description,price])

    const editProduct = (event) =>
    {
        event.preventDefault()
        const updatedproduct = 
        {
            name : name, 
            description : description,
            price : price,
            image : image,
            category : category,
            countInStock : countInStock
        }

        dispatch(updateProduct(match.params.productid, updatedproduct))
    }

    
    return (
        <>
        <h2 className="text-center mt-3">Update Product</h2>
        {loading && (<Load />)}
        {error && (<h2>Error....</h2>)}

        {updateloading && (<Load />)}
        {success && (<alert>Product Updated Successfully</alert>)}
        {updateerror && (<h1>Error....</h1>)}
       {product &&(<div>
       <Card className="p-3 mt-3 mb-2">
        <Form onSubmit={editProduct} >
                <Form.Group className="mb-3" >
                    <Form.Label>Product Name</Form.Label>
                    <Form.Control type="text" value={name} onChange={nameHandler} placeholder="Enter product name" /> 
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Description</Form.Label>
                    <Form.Control type="text" value={description} onChange={descriptionHandler} placeholder="Description" />
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Price</Form.Label>
                    <Form.Control type="Number" value={price} onChange={priceHandler}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Image</Form.Label>
                    <Form.Control type="text" value={image} placeholder="Image Url" onChange={imageHandler}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Stocks</Form.Label>
                    <Form.Control type="Number" value={countInStock} onChange={countInStockHandler}/>
                </Form.Group>

                <Form.Group className="mb-3">
                    <Form.Label>Category</Form.Label>
                    <Form.Control type="text" value={category} onChange={categoryHandler}/>
                </Form.Group>

                {
                    isActive
                    ?  <Button className="mb-3" variant="primary" type="submit">Update Product</Button>
                    :  <Button  className="mb-3" variant="danger" disabled >Update Product</Button>
                }
            </Form>
            </Card>
       </div>)}
        </>
    )
}

export default UpdateProduct