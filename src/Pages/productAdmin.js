import React, { useEffect, useState } from 'react'
import { Table, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'

const ProductA= () =>
{

    const [allProducts, setAllProducts] = useState([])
    const [update, setUpdate] = useState("")

    useEffect(()=>
    {
        fetch("https://infinite-plains-24207.herokuapp.com/api/products")
        .then(res => res.json())
        .then(data =>
        {
            console.log(data)
            setAllProducts(data)
        })

    },[update])


    function archive(productId)
    {
        fetch(`https://infinite-plains-24207.herokuapp.com/api/products/archive/${productId}`,
        {
            method : "PUT",
            headers : 
            {
                "Authorization" : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res =>res.json())
        .then(data =>
            {
                console.log(data)
                setUpdate({})
            })
    }

    function activate(productId)
    {
        fetch(`https://infinite-plains-24207.herokuapp.com/api/products/activate/${productId}`,{
            method : "PUT",
            headers :
            {
                "Authorization" : `Bearer ${localStorage.getItem(`token`)}`
            }
        })
        .then(res=> res.json())
        .then(data =>
        {
            console.log(data)
            setUpdate({})
        })
    }

    let productsRow = allProducts.map(products =>
        {
            return(
                <tr key={products._id}>
                    <td>{products._id}</td>
                    <td>{products.name}</td>
                    <td>{products.countInStock}</td>
                    <td className={products.isActive ? "text-success" : "tex-danger"}>{products.isActive ? "Available" : "Not Available"}</td>
                    <td>
                    {
                        products.isActive
                        ? <Button variant="danger" className="mx-2" onClick={()=> archive(products._id)}>Archive</Button>
                        : <Button variant="success" className="mx-2" onClick={()=> activate(products._id)}>Activate</Button>
                    }
                    </td>
                    <td><Link className="btn btn-primary" to={`/editproduct/${products._id}` }>Update Product</Link></td>
                </tr>
             
            )
        })

    return (
        <>
        <h1 className='text-center mt-2'>Admin Dashboards</h1>
        <Table striped bordered hover size="sm">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Product Name</th>
                    <th>Stocks</th>
                    <th>Status</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                {productsRow}
            </tbody>
            </Table>
        </>
    )
}

export default ProductA