import React, {useState, useEffect} from 'react'
import { Redirect } from 'react-router-dom'
import Swal from 'sweetalert2'

import { Form, Button} from 'react-bootstrap'

const Register = () =>
{
    const [firstName, setFirstName] = useState('')
    const [lastName, setLastName] = useState('')
    const [password, setPassword] = useState('')
    const [passwordConfirm, setPasswordConfirm] = useState('')
    const [email, setEmail] = useState('')
    const [address,setAddress] = useState('')
    const [isActive, setIsActive] = useState(true)
    const [willRedirect, setWillRedirect] = useState(false)

    const firstNameHandler = event =>
    {
        setFirstName(event.target.value)
    }
    const lastNameHandler = event =>
    {
        setLastName(event.target.value)
    }

    const passwordHandler = event =>
    {
        setPassword(event.target.value)
    }

    const emailHandler = event =>
    {
        setEmail(event.target.value)
    }

    const passwordConfirmHandler = event =>
    {
        setPasswordConfirm(event.target.value)
    }

    const addressHandler = event =>
    {
        setAddress(event.target.value)
    }

    useEffect(()=>
    {
        if(firstName !== "" && lastName !== "" && password !== "" && email !== "" && passwordConfirm !== "" && address !== "")    
        {
            if(password === passwordConfirm)
            {
                setIsActive(true)
            }
            else
            {
                <alert>Password do not match</alert>
            }  
        }
        else
        {
            setIsActive(false)
        }
    },[firstName, lastName, password, passwordConfirm, email, address])

    const submitHandler = event =>
    {
        event.preventDefault()
        setEmail('')
        setPassword('')
        setFirstName('')
        setLastName('')
        setAddress('')

        fetch("https://infinite-plains-24207.herokuapp.com/api/register", {
            method : "POST",
            headers : 
            {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                firstName : firstName,
                lastName : lastName,
                email : email,
                password : password,
                address : address
            })
        })
        .then(res => res.json())
        .then(data =>
            {
                if(data.message)
                {
                    Swal.fire(
                        {
                            icon : "error",
                            title : "Registration Failed",
                            text : data.message
                        }
                    )
                }
                else
                {
                    Swal.fire(
                        {
                            icon: "success",
                            title: "Registered Succesfully",
                            text: "Thank you for registering"
                        }
                    )
                    setWillRedirect(true)
                }
            })
    }
    return(
        willRedirect
        ? <Redirect to="/login" />
        :
        <div class="signup-form">
        <Form onSubmit={submitHandler}>
            <h2>Register</h2>
            <p className="hint-text">Create your account. It's free and only takes a minute.</p>
            <div className="form-group">
                <div className="row">
                    <div className="col"><input type="text" value={firstName} onChange={firstNameHandler} className="form-control" name="first_name" placeholder="First Name" required="required"/></div>
                    <div className="col"><input type="text" value={lastName} onChange={lastNameHandler} className="form-control" name="last_name" placeholder="Last Name" required="required"/></div>
                </div>        	
            </div>
            <div className="form-group">
                <input type="email" className="form-control" value={email} onChange={emailHandler} name="email" placeholder="Email" required="required" />
            </div>
            <div className="form-group">
                <input type="password" value={password} onChange={passwordHandler} className="form-control" name="password" placeholder="Password" required="required" />
            </div>
            <div className="form-group">
                <input type="password" value={passwordConfirm} onChange={passwordConfirmHandler} className="form-control" name="confirm_password" placeholder="Confirm Password" required="required"/>
            </div>      
            <div className="form-group">
                <input type="text" className="form-control" value={address} onChange={addressHandler} name="address" placeholder="Address" required="required" />
            </div>  
            <div className="form-group">
                <Form.Label className="form-check-label"><input type="checkbox" required="required"/> I accept the <a href="/">Terms of Use</a> &amp; <a href="/">Privacy Policy</a></Form.Label>
            </div>
            {
                isActive
                ? <div className="form-group">
                <Button type="submit" className="btn btn-success btn-lg btn-block">Register Now</Button>
                </div>
                : <div className="form-group">
                <Button type="submit" className="btn btn-success btn-lg btn-block" disabled>Register Now</Button>
                </div> 
            }
        </Form>
        <div className="text-center">Already have an account? <a href="/login" className="text-body">Sign in</a></div>
        </div>
    )
}

export default Register