import React from 'react'
import { Carousel, Container} from 'react-bootstrap'
import Cards from '../Components/Card'
import { Link } from 'react-router-dom'
import '../styles.css'
import Category from '../Components/Categories'

const Home = () =>
{
    return(
    <>
    <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://www.sneaker4shoes.com/media/wysiwyg/infortis/slideshow/adidas-banner-1.jpg"
            alt="First slide"
          />
          <Carousel.Caption>
            <h3 className='text-body text-uppercase heading'>Save up to 35% on Flex Pricing Shop Products </h3>
            <p>Adidas NMD R1 PrimeKnit Zebra</p>
            <Link to='/register' className="btn button primary-button">Now Available</Link>

          </Carousel.Caption>

        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="https://heybusiness.kcom.com/media/254124/iphone-xr-banner.jpg"
            alt="Second slide"
          />
      
          <Carousel.Caption>
            <h3 className='text-uppercase heading'>iPhone XR from $499</h3>
            <p className='paragraphs'>Trade in your current Iphone and upgrade to new one</p>
            <Link to='/register' className="btn button primary-button">Limited Time</Link>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>

      <Container className='section-page'>
             <h2 className='mt-5 text-uppercase text-center project'>What Do We Offer</h2>
             <Cards />
      </Container>

      <Container className='section-page'>
      <h2 className='mt-5 text-uppercase text-center project'>Categories of the month</h2>
            <Category />
      </Container>

      <div className="p-3 mt-2" style={{backgroundColor :"#000"}}>
            <h2 className='text-center text-white'>Copyright 2021</h2>
      </div>
     
      </>
    )
}

export default Home