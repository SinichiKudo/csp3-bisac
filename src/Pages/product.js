import React, { useEffect} from 'react'
import Product from '../Components/Product'
import {Row} from 'react-bootstrap'
import Filter from '../Components/Filter'
import { useDispatch, useSelector } from 'react-redux'
import { getAllProducts } from '../actions/productActions'
import Load from '../Components/Loader'

const ProductPage = () =>
{
    const getallproductsstate = useSelector(state=>state.getAllProductsReducer)
    const {loading, products, error} = getallproductsstate
    console.log(products)

    const dispatch = useDispatch()


    useEffect(()=>
    {
        dispatch(getAllProducts())
    },[])

    return (
        <>
        <h1 className="text-center mt-3">PRODUCTS</h1>
        <Filter/>
        <Row>
        {
            loading ? (
                <Load />
               
            ) : error ? (
               <h1>Something went wrong</h1> 
            ) : (
                products.map(product=>
                {

                    if(product.isActive === true)
                    {
                        return <Product key={product._id} productProps={product} />
                    }
                    
                })
            )
        }
        </Row>
        </>
    )
}

export default ProductPage