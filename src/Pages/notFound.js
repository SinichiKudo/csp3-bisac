import React from 'react'
import Banner from '../Components/Banner'

const NotFound = () =>
{
    const bannerComponents =
    {
        title : "PAGE NOT FOUND",
        description : "You are in the wrong address, please go Back to Home Page",
        label : "Home Page",
        destination : '/'
    }
    return(
        <Banner bannerProps={bannerComponents}/>
    )
}

export default NotFound