import React, {useState, useEffect, useContext} from 'react'
import {Form, Button} from 'react-bootstrap'
import Swal from 'sweetalert2'
import { Redirect } from 'react-router-dom'
import UserContext from '../userContext'

const Login = () =>
{
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [isActive, setIsActive] = useState(true)
    const [willRedirect, setWillRedirect] = useState(false)
    const{user, setUser} = useContext(UserContext)

    const emailHandler = event =>
    {
        setEmail(event.target.value)
    }

    const passwordHandler = event =>
    {
        setPassword(event.target.value)
    }

    useEffect(()=>
    {
        if(email !== "" && password !== "")
        {
            setIsActive(true)
        }
        else
        {
            setIsActive(false)
        }
    },[email, password])

    const submitHandler = event =>
    {
        event.preventDefault()
        setEmail('')
        setPassword('')

        fetch("https://infinite-plains-24207.herokuapp.com/api/login",{
            method : "POST",
            headers : 
            {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email : email,
                password : password
            })
        })
        .then(res => res.json())
        .then(data =>
            {
                console.log(data)

                if(data.message)
                {
                    Swal.fire(
                        {
                            icon : "error",
                            title : "Login Failed",
                            text : data.message
                        }
                    )
                }
                else
                {
                    localStorage.setItem('token', data.accessToken)
                    fetch("https://infinite-plains-24207.herokuapp.com/api/users",
                    {
                        headers:
                        {
                            "Authorization" : `Bearer ${data.accessToken}`
                        }
                    })
                    .then(res =>res.json())
                    .then(data =>
                    {
                        localStorage.setItem('email', data.email)
                        localStorage.setItem('isAdmin', data.isAdmin)
                        setUser({
                            email : data.email,
                            isAdmin : data.isAdmin
                        })

                        Swal.fire(
                            {
                                icon : "success",
                                title : "Login Successfully",
                                text : `Thank you for logging in, ${data.firstName}`
                            }
                        )
                        setWillRedirect(true)
                    })
                }
            })
    }


    return(
        user.email || willRedirect
        ? <Redirect to='/' />
        :
	<div className="container h-100 mt-5">
		<div className="d-flex justify-content-center h-100">
			<div className="user_card">
				<div className="d-flex justify-content-center">
					<div className="brand_logo_container">
						<img src="https://www.kindpng.com/picc/m/361-3618010_circle-png-circle-logo-template-png-transparent-png.png" className="brand_logo" alt="Logo" />
					</div>
				</div>
				<div className="d-flex justify-content-center form_container">
					<Form onSubmit={submitHandler}>
						<div className="input-group mb-3">
							<div className="input-group-append">
								<span className="input-group-text"><i className="fas fa-user"></i></span>
							</div>
							<input type="text" name="" className="form-control input_user" value={email} onChange={emailHandler} placeholder="username"/>
						</div>
						<div className="input-group mb-2">
							<div className="input-group-append">
								<span className="input-group-text"><i className="fas fa-key"></i></span>
							</div>
							<input type="password" name="" className="form-control input_pass" value={password} onChange={passwordHandler} placeholder="password"/>
						</div>
							<div className="d-flex justify-content-center mt-3 login_container">
                            {
                                isActive
                                ? <Button type="submit" name="button"  className="btn login_btn">Login</Button>
                                : <Button type="submit" disabled  className="btn login_btn">Login</Button>
                            }
				            </div>
					</Form>
				</div>
		
				<div className="mt-4">
					<div className="d-flex justify-content-center links">
						Don't have an account? <a href="/register" className="ml-2">Sign Up</a>
					</div>
					<div className="d-flex justify-content-center links">
						<a href="/">Forgot your password?</a>
					</div>
				</div>
			</div>
		</div>
	</div>
    )
}

export default Login