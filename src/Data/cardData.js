const cardComponents = 
[
    {
        img : "https://images.unsplash.com/photo-1460925895917-afdab827c52f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=702&q=80",
        name : "Credible and Affordable",
        description : "Reliability is the probability that a product or equipment will perform satisfactorily for a given time under normal conditions of use",
        price : 1200

    },
    {
        img : "https://images.unsplash.com/photo-1460925895917-afdab827c52f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=702&q=80",
        name : "Credible and Affordable",
        description : "Reliability is the probability that a product or equipment will perform satisfactorily for a given time under normal conditions of use",
        price : 1300
    },
    {
        img : "https://images.unsplash.com/photo-1460925895917-afdab827c52f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=702&q=80",
        name : "Credible and Affordable",
        description : "Reliability is the probability that a product or equipment will perform satisfactorily for a given time under normal conditions of use",
        price : 1400
    },
    {
        img : "https://images.unsplash.com/photo-1460925895917-afdab827c52f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=702&q=80",
        name : "Credible and Affordable",
        description : "Reliability is the probability that a product or equipment will perform satisfactorily for a given time under normal conditions of use",
        price : 1400
    }
]

export default cardComponents;