import axios from 'axios'

export const getAllProducts = () =>dispatch=>
{
    dispatch({type : 'GET_PRODUCTS_REQUEST'})

    axios.get('https://infinite-plains-24207.herokuapp.com/api/products').then(res=>
    {
        console.log(res)

        dispatch({type : 'GET_PRODUCTS_SUCCESS', payload : res.data})
    }).catch(err=>
        {
            console.log(err)
            dispatch({type : 'GET_PRODUCTS_SUCCESS', payload : err})
        })
}

export const getProductById = (productid) =>dispatch=>
{
    dispatch({type : 'GET_PRODUCTS_REQUEST'})

    axios.post('https://infinite-plains-24207.herokuapp.com/api/getproductbyid', {productid}).then(res=>
    {
        dispatch({type : 'GET_PRODUCTBYID_SUCCESS', payload : res.data})
    }).catch(err=>
    {
        console.log(err)
        dispatch({type : 'GET_PRODUCTBYID_FAILED', payload : err})
    })
}

export const filterProducts = (searchKey, sortKey, category) => dispatch=>{

    var filteredproducts ;
    dispatch({type : 'GET_PRODUCTS_REQUEST'})
    axios.get("https://infinite-plains-24207.herokuapp.com/api/products").then(res=>
    {
        
        filteredproducts = res.data
        console.log(filteredproducts)
        if(searchKey)
        {
            filteredproducts = res.data.filter(product => {return product.name.toLowerCase().includes(searchKey)})
        }

        if(sortKey !== 'best')
        {
            if(sortKey === 'hp')
            {
                filteredproducts = res.data.sort((a, b)=>
                {
                    return -a.price + b.price
                })
            }
            else
            {
                filteredproducts = res.data.sort((a, b)=>
                {
                    return a.price - b.price
                })
            }
        }

        if(category !== 'all')
        {
            filteredproducts = res.data.filter(product => {return product.category.toLowerCase().includes(category)})
        }

        dispatch({type : 'GET_PRODUCTS_SUCCESS', payload : filteredproducts})

    }).catch(err=>
    {
        dispatch({type : 'GET_PRODUCTS_FAILED'})
    })
}

export const addProductReview = (review, productid) =>dispatch=>{

    dispatch({type : 'ADD_PRODUCT_REVIEW_REQUEST'})

    axios.post("https://infinite-plains-24207.herokuapp.com/api/products/addreview", {review,productid}).then(res=>
    {
        console.log(res);
        dispatch({type : 'ADD_PRODUCT_REVIEW_SUCCESS'})
       
    }).catch(err=>
    {
        dispatch({type : 'ADD_PRODUCT_REVIEW_FAILED'})
    })
}

export const updateProduct = (productid, updatedproduct) => dispatch =>
{
    dispatch({type : 'UPDATE_PRODUCT_REQUEST'})

    axios.post("https://infinite-plains-24207.herokuapp.com/api/products/updateproduct", {productid, updatedproduct}).then(res =>
    {
        console.log(res)
        dispatch({type: 'UPDATE_PRODUCT_SUCCESS'})
        window.location.href='/productA'
    }).catch(err =>
    {
        dispatch({type: 'UPDATE_PRODUCT_FAILED'})
    })
}


