import axios from "axios"

export const placeOrder = (token, subtotal) => (dispatch, getState)=>
{
    const user = getState().loginReducer.currentUser
    const cartItems = getState().cardReducer.cartItems

    dispatch({type : 'PLACE_ORDER_REQUEST'})

    axios.post('https://infinite-plains-24207.herokuapp.com/api/orders/placeOrder', {token, subtotal, user, cartItems}).then(res=>
    {
        dispatch({type : 'PLACE_ORDER_SUCCESS'})
    }).catch(err =>
    {
        dispatch({type : 'PLACE_ORDER_FAILED'})
    })
}