import React, {useState} from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import { Route, Switch } from 'react-router-dom';
import {Container} from 'react-bootstrap'
import {UserProvider} from '../src/userContext'


//Boostrap CSS
import 'bootstrap/dist/css/bootstrap.min.css';
import './styles.css'

//Components and Pages
import NavBar from './Components/NavBar';
import Home from './Pages/home';
import Register from './Pages/register';
import Login from './Pages/login';
// import NotFound from './Pages/notFound';
import ProductPage from './Pages/product';
import AddProducts from './Pages/addProduct';
import ProductDescription from './Pages/productDes';
import CartScreen from './Pages/cartScreen';
import ProductA from './Pages/productAdmin';
import UpdateProduct from './Pages/editProduct';


function App() {

  const [user, setUser] = useState(
  {
    email : localStorage.getItem('email'),
    isAdmin : localStorage.getItem('isAdmin') === 'true'
  })

  function unsetUser()
  {
      localStorage.clear()
  }


  return (
    <>
      <UserProvider value={{user,setUser,unsetUser}}>
        <Router>
          <NavBar />
            <Switch>
              <Route exact path="/" component={Home} />
            </Switch>
            <Container >
                <Switch> 
                    <Route exact path="/register" component={Register} />
                    <Route exact path="/login" component={Login} />
                    <Route exact path="/product" component={ProductPage} />
                    <Route exact path="/addProducts" component={AddProducts} />
                    <Route exact path='/product/:id' component={ProductDescription} />
                    <Route exact path="/productA" component={ProductA} />
                    <Route exact path='/cart' component={CartScreen} />
                    <Route exact path='/editProduct/:productid' component={UpdateProduct} />
                    {/* <Route component={NotFound} /> */}
                </Switch>
            </Container>
        </Router>
      </UserProvider>
    </>
  );
}

export default App;
