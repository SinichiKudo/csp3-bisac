import React, {useContext} from 'react'
import {Navbar, Nav} from 'react-bootstrap'
import { NavLink, Link } from 'react-router-dom'
import UserContext from '../userContext'
import { useSelector } from 'react-redux'



const NavBar  = () =>
{

    const cartReducer = useSelector(state=>state.cartReducer)

    const {cartItems} = cartReducer
    const {user,unsetUser, setUser} = useContext(UserContext)

    const logout = ()=>
    {
        unsetUser()
        setUser({
            email : null,
            isAdmin : null
        })
    }

    return (
        <Navbar bg="dark" variant="dark" expand="lg">
            <Navbar.Brand as={Link}  to="/" href="#home">FLEX PRICING</Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="ml-auto">
            <Nav.Link as={NavLink} to="/" href="#home">Home</Nav.Link>
            {
                user.email
                    ?
                     user.isAdmin
                     ? <>
                        <Nav.Link as={NavLink} to="/productA" href="#productA">Product Admin</Nav.Link>
                        <Nav.Link as={NavLink} to="/addProducts" href="#addProducts">Add Products</Nav.Link>
                        <Nav.Link as={NavLink} to="/editProduct/:productid" >Update Product</Nav.Link>
                       <Nav.Link as={NavLink} to="/login" onClick={logout} href="#logout">Logout</Nav.Link>
                      </>
                     
                     :  <>
                     <Nav.Link as={NavLink} to="/product" href="#product">Products</Nav.Link>
                     <Nav.Link as={NavLink} to="/login" onClick={logout} href="#logout">Logout</Nav.Link>
                     <Nav.Link as={NavLink} to="/cart" href="#cart"><i class="fas fa-shopping-cart"></i>{cartItems.length}</Nav.Link>
                     </>
                
                    :
                    <> 
                    <Nav.Link as={NavLink} to="/product" href="#product">Products</Nav.Link>
                    <Nav.Link as={NavLink} to="/login" href="#login">Login</Nav.Link>
                    </>
            }    
            </Nav>
            </Navbar.Collapse>
         </Navbar>
    )
}

export default NavBar;