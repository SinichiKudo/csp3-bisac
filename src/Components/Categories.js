import React from 'react'
import { Row, Col, Card, Image} from 'react-bootstrap'
import {Link} from 'react-router-dom'

const Category = () =>
{
    return (
        <Row className='mt-3'>
        <Col xs={12} md={4} className="mb-3">
            <Card className="cardDeck">
            <Image src="https://images.unsplash.com/photo-1496181133206-80ce9b88a853?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=751&q=80" fluid />
                <Card.Body>
                    <Card.Title className='text-center'>
                        <h2>Electronics</h2>
                    </Card.Title>
                    <div className='text-center'> <Link to='/product' className="btn button primary-button">Shop Now</Link></div>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}  className="mb-3">
            <Card className="cardDeck">
            <Image src="https://images.unsplash.com/photo-1542291026-7eec264c27ff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80" fluid />
                <Card.Body>
                    <Card.Title className='text-center'>
                        <h2>Shoes</h2>
                    </Card.Title>
                    <div className='text-center'> <Link to='/product' className="btn button primary-button">Shop Now</Link></div>
                </Card.Body>
            </Card>
        </Col>
        <Col xs={12} md={4}  className="mb-3">
            <Card  className="cardDeck">
            <Image src="https://images.unsplash.com/photo-1585060544812-6b45742d762f?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=759&q=80" fluid />
                <Card.Body>
                    <Card.Title className='text-center'>
                        <h2>Mobiles</h2>
                    </Card.Title>
                    <div className='text-center'> <Link to='/product' className="btn button primary-button">Shop Now</Link></div>
                </Card.Body>
            </Card>
        </Col>
    </Row>

    )
}

export default Category