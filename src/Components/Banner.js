import React from 'react'
import {Row, Col,Jumbotron } from 'react-bootstrap'
import { Link } from 'react-router-dom'

const Banner = ({bannerProps}) =>
{


    return (
        <Row>
            <Col>
                <Jumbotron>
                    <h1>{bannerProps.title}</h1>
                    <p >{bannerProps.description}</p>
                    <Link to={bannerProps.destination} className="btn btn-primary">{bannerProps.label}</Link>
                </Jumbotron>
            </Col>
        </Row>
    )
}

export default Banner