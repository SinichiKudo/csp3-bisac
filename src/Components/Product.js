import React, { useContext } from "react";
import { Col, Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import Rating from "react-rating";
import UserContext from "../userContext";

const CardProduct = ({ productProps }) => {

  const {user} = useContext(UserContext)
  return (
    <>
      <Col xs={12} lg={4} className="mb-3 mt-3">
        <Card className="productHeight cards">
          <Link to={`product/${productProps._id}`}>
            <Card.Img variant="top" src={productProps.image} />
            <Card.Body>
              <Card.Title>{productProps.name}</Card.Title>
              <Rating
                initialRating={productProps.rating}
                emptySymbol= "far fa-star fa-1x" aria-hidden="true"
                fullSymbol="fas fa-star fa-1x"
                readonly =  {true}
              />
              <Card.Text>Price: ${productProps.price}</Card.Text>
              {
                user.email
                ? <Button variant="primary">Add to Cart</Button>
                : <Link to='/register' className="btn btn-danger">Register to View</Link>
              }
            </Card.Body>
          </Link>
        </Card>
      </Col>
    </>
  );
};

export default CardProduct;
