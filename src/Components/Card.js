import React from 'react'
import {Row, Col, Card, Image} from 'react-bootstrap'
import '../styles.css'

const Cards = () =>
{
    return(
        <Row className='mt-3'>
            <Col xs={12} md={4} className="mb-3">
                <Card className="cardDeck">
                <Image src="https://images.unsplash.com/photo-1586880244543-0528a802be97?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80" fluid />
                    <Card.Body>
                        <Card.Title className='text-center'>
                            <h2>Affordable and Credible Products</h2>
                        </Card.Title>
                        <Card.Text>
                             We sell products that are credible and affordable to help you create good moments that become lasting memories. Join the community to create long lasting memories. 
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}  className="mb-3">
                <Card  className="cardDeck">
                <Image src="https://images.unsplash.com/photo-1610886147082-2f8dfa0c1f1b?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80" fluid />
                    <Card.Body>
                        <Card.Title className='text-center'>
                            <h2>Fast Delivery of Products</h2>
                        </Card.Title>
                        <Card.Text>
                                Loading, transporting, and delivering items to clients or businesses in a safe, timely manner.  The charges are correct, and the customer is satisfied.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
            <Col xs={12} md={4}  className="mb-3">
                <Card className="cardDeck">
                <Image src="https://images.unsplash.com/photo-1484704849700-f032a568e944?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=750&q=80" fluid />
                    <Card.Body>
                        <Card.Title className='text-center'>
                            <h2>Top Quality of Products</h2>
                        </Card.Title>
                        <Card.Text>
                        A Reliable Product is something that provides a consistent, predictable experience when used or observed. Ingineous, quality products designed to last a lifetime.
                        </Card.Text>
                    </Card.Body>
                </Card>
            </Col>
        </Row>
       
    )
}

export default Cards