import React, { useState } from 'react'
import {Row, Col, Button} from 'react-bootstrap'
import { useDispatch } from 'react-redux'
import { filterProducts } from '../actions/productActions'

const Filter = () =>
{

    const [search, setSearch] = useState('')
    const [sort, setSort] = useState('best')
    const [category, setCategory] = useState('all')

    const dispatch = useDispatch()

    const searchHandler = event =>
    {
        setSearch(event.target.value)
    }

    const sortHandler = event =>
    {
        setSort(event.target.value)
    }

    const categoryHandler = event =>
    {
        setCategory(event.target.value)
    }

    return(
        <div className="text-center">
        <Row className="mt-3">
            <Col md={3}>
                <input className="form-control" value={search} onChange={searchHandler} type="text" placeholder="Enter product" />
            </Col>
            <Col md={3}>
               <select value={sort} onChange={sortHandler} className="form-control">
                   <option value="best">Best Seller</option>
                   <option value="lp">Lowest Price</option>
                   <option value="hp">Highest Price</option>
               </select>
            </Col>
            <Col md={3}>
               <select value={category} onChange={categoryHandler} className="form-control">
                   <option value="all">All</option>
                   <option value="electronics">Electronics</option>
                   <option value="mobiles">Mobiles</option>
                   <option value="shoes">Shoes</option>
               </select>
            </Col>
            <Col md={3}>
               <Button variant="primary" onClick={()=>{dispatch(filterProducts(search, sort, category))}} className="btn-block">Search</Button>
            </Col>
        </Row>
        </div>
    )
}

export default Filter