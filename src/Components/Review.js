import React, {useState} from 'react'
import Rating from 'react-rating'
import { Button } from 'react-bootstrap'
import { useDispatch} from 'react-redux'
import { addProductReview } from '../actions/productActions'
import Swal from 'sweetalert2'

const Review = ({product}) =>
{
    const [rating, setRating]= useState(5)
    const [comment, setComment] = useState('')
    const dispatch = useDispatch()

    const ratingHandler = event =>
    {
        setRating(event)
    }

    const commentHandler = event =>
    {
        setComment(event.target.value)
    }

    const sendReview = () =>
    { 
        // const currentUser = JSON.parse(localStorage.getItem('email'))

        // let alreadyreviewed
            const review ={
                rating : rating,
                comment : comment
            }
            dispatch(addProductReview(review, product._id))

            Swal.fire({
                icon : "success",
                title : "You have submitted your review",
                text : "Thank you"
            })
        
    }
      


    return(
        <>
        <h2>Add Reviews</h2>
        <Rating
            style = {{color : "blue"}}
            initialRating={rating}
            emptySymbol= "far fa-star fa-1x" aria-hidden="true"
            fullSymbol="fas fa-star fa-1x"
            onChange={ratingHandler}
        />

        <input className="form-control my-3" type="text" value={comment} onChange={commentHandler} placeholder="Give a comment"/>
        <Button variant="primary" onClick={sendReview} >Send Review</Button>

        <h2 className="mt-3">Latest Reviews</h2>

        {product.reviews  && (product.reviews.map(review =>
        {
           return <div>
            <Rating
            style = {{color : "blue"}}
            initialRating={review.rating}
            emptySymbol= "far fa-star fa-1x"
            fullSymbol="fas fa-star fa-1x"
            readonly /> 

            <p>{review.comment}</p>
            </div>
        }))}
        
        </>

    )
}

export default Review